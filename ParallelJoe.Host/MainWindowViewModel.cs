﻿using System.Reactive.Linq;
using Microsoft.Owin.Hosting;
using ReactiveUI;

namespace ParallelJoe.Host
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            WebApp.Start<Startup>("");
        }
    }
}