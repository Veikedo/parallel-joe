﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using Microsoft.Owin.Hosting;
using ParallelJoe.Host.Hubs;
using Expression = NCalc.Expression;

namespace ParallelJoe.Host
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private Stopwatch _sw = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
            WebApp.Start<Startup>("http://localhost:8080/");
            WorkerHub.TaskCompleted +=
                (guid, d) => Dispatcher.InvokeAsync(() =>
                {
                    _sw.Stop();
                    Result.Text = d.ToString(CultureInfo.CurrentCulture);
                    LogTextBox.Text += $"На клиентах задача сделана за {_sw.ElapsedMilliseconds} миллисекунд\r\n";

                    Overlay.Visibility = Visibility.Collapsed;
                });
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var from = double.Parse(From.Text);
            var to = double.Parse(To.Text);

            Overlay.Visibility = Visibility.Visible;

            _sw.Restart();
            await WorkerHub.CalcSquareAsync(Function.Text, from, to).ConfigureAwait(true);

            var sw = Stopwatch.StartNew();
            var res = CalcSquare(Function.Text, from, to);
            sw.Stop();

            ServerResult.Text = res.ToString(CultureInfo.CurrentCulture);
            LogTextBox.Text += $"На сервер задача сделана за {sw.ElapsedMilliseconds} миллисекунд\r\n";
        }

        private static Double CalcSquare(string function, double from, double to)
        {
            const double eps = 1e-3;

            double result = 0;
            var e = new Expression(function);
            for (double x = from; x < to; x += eps)
            {
                e.Parameters["x"] = x;
                result += (double)e.Evaluate() * eps;
            }

            return result;
        }
    }
}