﻿using System;
using System.Threading.Tasks;

namespace ParallelJoe.Host.Hubs
{
    public interface IWorkerHubClient
    {
        Task CalcSquareAsync(Guid taskId, string func, double from, double to);
    }
}