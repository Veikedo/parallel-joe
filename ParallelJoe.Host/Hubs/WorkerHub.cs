﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Nito.AsyncEx;

namespace ParallelJoe.Host.Hubs
{
    public class SquareCalcTask
    {
        public SquareCalcTask(double @from, double to)
        {
            From = @from;
            To = to;
            Subtasks = new Dictionary<Guid, SquareCalcTask>();
            TaskId = Guid.NewGuid();
        }

        public Guid TaskId { get; }
        public Guid ParentTaskId { get; set; }
        public double Result { get; set; }
        public double From { get; }
        public double To { get; }
        public Dictionary<Guid, SquareCalcTask> Subtasks { get; }
    }

    public class WorkerHub : Hub<IWorkerHubClient>, IWorkerHub
    {
        private static readonly Dictionary<string, IWorkerHubClient> Workers;
        private static readonly Dictionary<Guid, SquareCalcTask> Subtasks;
        private static readonly Dictionary<Guid, SquareCalcTask> Tasks;
        private static readonly AsyncLock Mutex = new AsyncLock();

        static WorkerHub()
        {
            Subtasks = new Dictionary<Guid, SquareCalcTask>();
            Tasks = new Dictionary<Guid, SquareCalcTask>();
            Workers = new Dictionary<string, IWorkerHubClient>();
        }

        public static async Task CalcSquareAsync(string func, double from, double to)
        {
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                var task = new SquareCalcTask(from, to);
                Tasks[task.TaskId] = task;

                var part = (to - from)/Workers.Count;
                double chunk = 0;

                foreach (var worker in Workers.Values)
                {
                    var subtask = new SquareCalcTask(from, to) {ParentTaskId = task.TaskId};
                    task.Subtasks[subtask.TaskId] = subtask;
                    Subtasks[subtask.TaskId] = subtask;

                    await worker.CalcSquareAsync(subtask.TaskId, func, chunk, chunk + part).ConfigureAwait(false);
                    chunk += part;
                }
            }
        }

        public async Task ReportResultAsync(Guid taskId, double square)
        {
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                var task = Tasks[Subtasks[taskId].ParentTaskId];
                task.Result += square;

                task.Subtasks.Remove(taskId);
                Subtasks.Remove(taskId);

                if (task.Subtasks.Count == 0)
                {
                    OnTaskCompleted(task.TaskId, task.Result);
                    Tasks.Remove(task.TaskId);
                }
            }
        }

        public static event Action<Guid, double> TaskCompleted;

        public override async Task OnDisconnected(bool stopCalled)
        {
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                Workers.Remove(Context.ConnectionId);
            }
        }

        public override async Task OnConnected()
        {
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                Workers[Context.ConnectionId] = Clients.Caller;
            }
        }

        private static void OnTaskCompleted(Guid arg1, double arg2)
        {
            TaskCompleted?.Invoke(arg1, arg2);
        }
    }
}