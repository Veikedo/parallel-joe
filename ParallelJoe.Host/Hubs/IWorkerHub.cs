using System;
using System.Threading.Tasks;

namespace ParallelJoe.Host.Hubs
{
    public interface IWorkerHub
    {
        Task ReportResultAsync(Guid taskId, double square);
    }
}