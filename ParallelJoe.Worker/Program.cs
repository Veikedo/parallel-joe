﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using NCalc;

namespace ParallelJoe.Worker
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var hubConnection = new HubConnection("http://localhost:8080/signalr");
            IHubProxy hubProxy = hubConnection.CreateHubProxy("WorkerHub");

            hubProxy.On("CalcSquareAsync", (Guid taskId, string func, double from, double to) =>
            {
                Console.WriteLine($"Task received: from {from}, to {to}");
                var square = CalcSquare(func, @from, to);
                Console.WriteLine("Task done");
                hubProxy.Invoke("ReportResultAsync", taskId, square);
            });

            // Console.WriteLine("Press Enter to start");
            // Console.ReadLine();
            hubConnection.Start();

            Console.WriteLine("Worker started. Press enter to exit.\r\nWaiting for tasks...");
            Console.ReadLine();

            Console.WriteLine("Worker stopped.");
            hubConnection.Stop();
        }

        private static Double CalcSquare(string function, double from, double to)
        {
            const double eps = 1e-3;

            double result = 0;
            var e = new Expression(function);
            for (double x = from; x < to; x += eps)
            {
                e.Parameters["x"] = x;
                result += (double) e.Evaluate()*eps;
            }

            return result;
        }
    }
}